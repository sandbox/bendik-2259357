<?php

/**
 * @file
 */


/**
 * Menu callback: display an overview of available types.
 */
function finn_ui_types_overview() {
  //drupal_add_css(drupal_get_path('module', 'commerce_product') . '/theme/commerce_product.admin.css');

  $header = array(
    t('Name'),
    t('Operations'),
  );

  $rows = array();

  // Loop through all defined product types.
  foreach (finn_types() as $type => $ad_type) {
    // Build the operation links for the current product type.
    $links = menu_contextual_links('finn-ad-type', 'admin/finn/ads/types', array(strtr($type, array('_' => '-'))));

    // Add the product type's row to the table's rows array.
    $rows[] = array(
      theme('finn_ad_type_admin_overview', array('ad_type' => $ad_type)),
      theme('links', array('links' => $links, 'attributes' => array('class' => 'links inline operations'))),
    );
  }

  // If no product types are defined...
  if (empty($rows)) {
    // Add a standard empty row with a link to add a new product type.
    $rows[] = array(
      array(
        'data' => t('There are no ad types defined.'),
        'colspan' => 2,
      )
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Builds an overview of a product type for display to an administrator.
 *
 * @param $variables
 *   An array of variables used to generate the display; by default includes the
 *     type key with a value of the product type array.
 *
 * @ingroup themeable
 */
function theme_finn_ad_type_admin_overview($variables) {
  $ad_type = $variables['ad_type'];

  $output = check_plain($ad_type['name']);
  $output .= ' <small>' . t('(Machine name: @type)', array('@type' => $ad_type['type'])) . '</small>';
  $output .= '<div class="description">' . filter_xss_admin($ad_type['description']) . '</div>';

  return $output;
}

/**
 * Form callback wrapper: create or edit a product type.
 *
 * @param $type
 *   The machine-name of the product type being created or edited by this form
 *     or a full product type array.
 *
 * @see commerce_product_product_type_form()
 */
function finn_ui_ad_type_form_wrapper($type) {
  if (is_array($type)) {
    $ad_type = $type;
  }
  else {
    $ad_type = finn_ad_type_load($type);
  }

  // Include the forms file from the Finn module.
  module_load_include('inc', 'finn_ui', 'includes/finn_ui.forms');

  return drupal_get_form('finn_ui_ad_type_form', $ad_type);
}