<?php

class FinnAdInlineEntityFormController extends EntityInlineEntityFormController {

  /**
   * Overrides EntityInlineEntityFormController::tableFields().
   */
  public function tableFields($bundles) {
    $fields = array();
    $fields['title'] = array(
      'type' => 'property',
      'label' => t('Title'),
      'weight' => 1,
    );
    $fields['finncode'] = array(
      'type' => 'property',
      'label' => t('Finn code'),
      'weight' => 2,
    );
    $fields['scraped'] = array(
      'type' => 'property',
      'label' => t('Last fetched'),
      'weight' => 3,
    );
    return $fields;
  }

  public function entityForm($entity_form, &$form_state) {
    global $user;

    $finn_ad = $entity_form['#entity'];
    //$extra_fields = field_info_extra_fields('finn_ad', $finn_ad->type, 'form');
    // Assign newly created ads to the current user.
    if (empty($finn_ad->finncode)) {
      $finn_ad->uid = $user->uid;
    }

    $entity_form['finncode'] = array(
      '#type' => 'textfield',
      '#title' => t('Finn.no code'),
      '#description' => t('Supply Finn.no\'s unique code for this ad.'),
      '#default_value' => $finn_ad->finncode,
      '#disabled' => !empty($finn_ad->finncode),
      '#maxlength' => 128,
      '#required' => TRUE,
      '#weight' => -10,
    );
    $entity_form['scrape'] = array(
      '#type' => 'checkbox',
      '#title' => t('Force data refresh'),
      '#description' => t('Refresh the data for this ad.'),
      '#access' => user_access('administer finn_ad entities'),
    );

    // Attach fields.
    $langcode = entity_language('finn_ad', $finn_ad);
    field_attach_form('finn_ad', $finn_ad, $entity_form, $form_state, $langcode);

    // And then hide them.
    foreach (element_children($entity_form) as $field_name) {
      if (!in_array($field_name, array('finncode', 'scrape'))) {
        $entity_form[$field_name]['#access'] = FALSE;
      }
    }

    if (empty($finn_ad->finncode)) {
      $entity_form['scrape'] =  array(
        '#type' => 'value',
        '#value' => 1,
      );
    }

    return $entity_form;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityFormValidate().
   */
  public function entityFormValidate($entity_form, &$form_state) {
    $finn_ad = $entity_form['#entity'];
    $parents_path = implode('][', $entity_form['#parents']);

    $values = drupal_array_get_nested_value($form_state['values'], $entity_form['#parents']);
    $finncode = trim($values['finncode']);

    if (!is_numeric($finncode)) {
      form_set_error($parents_path . '][finncode', t('The Finn.no code is not numeric.'));
    } else {

      if (empty($finn_ad->finncode) && (($existing_ad = finn_ad_load(trim($finncode))) !== FALSE)) {
        form_set_error($parents_path . '][finncode', t('The Finn.no ad %finncode already exists.', array('%finncode' => $finncode)));
      }
    }

    // Trim leading and trailing whitespace from the Finn code.
    drupal_array_set_nested_value($form_state['values'], array_merge($entity_form['#parents'], array('finncode')), $finncode);

    // Notify field widgets to validate their data.
    field_attach_form_validate('finn_ad', $finn_ad, $entity_form, $form_state);

    // If this is a new ad, it must be scraped and before it can be considered valid.
    $errors = form_get_errors();
    if (!empty($finn_ad->is_new) && empty($errors)) {
      global $user;

      // Set the ad's uid and finncode if it's being created at this time.
      if (!empty($finn_ad->is_new)) {
        $finn_ad->finncode = $finncode;
        $finn_ad->uid = $user->uid;
      }

      field_attach_submit('finn_ad', $finn_ad, $entity_form, $form_state);

      try {
        entity_save('finn_ad', $finn_ad);
        $finn_ad->_ief_skip_save = TRUE;

      } catch (Exception $e) {
        form_set_error($parents_path . '][finncode', $e->getMessage());
      }
    }
  }

  /**
   * Overrides EntityInlineEntityFormController::entityFormSubmit().
   */
  public function entityFormSubmit(&$entity_form, &$form_state) {
    parent::entityFormSubmit($entity_form, $form_state);

    $finn_ad = $entity_form['#entity'];
    $values = drupal_array_get_nested_value($form_state['values'], $entity_form['#parents']);

    if (isset($finn_ad->_ief_skip_save)) {
      $finn_ad->scraped = t('A moment ago');
      drupal_set_message(t('The ad `!code` was successfully fetched and saved.', array(
        '!code' => $finn_ad->finncode,
      )));
    } else if ($values['scrape']) {
      // Show a temporary message to the user.
      $finn_ad->scraped = t('Will be fetched when this form is saved.');

      // Mark this ad to be scraped, which is done from the entity controller.
      $finn_ad->scrape = TRUE;
    }
  }

  /**
   * Overrides EntityInlineEntityFormController::save().
   */
  public function save($entity, $context) {
    if (isset($entity->_ief_skip_save)) {
      return;
    }

    // Save the ad.
    try {
      entity_save('finn_ad', $entity);
    } catch (Exception $e) {
      $entity->finncode = NULL;

      drupal_set_message(t('An error occoured during Finn.no data acquisition: !exception', array(
        '!exception' => $e->getMessage(),
      )), 'error');
    }
  }
}