<?php

/**
 * @file
 * Forms for editing ad types.
 */


/**
 * Form callback: create or edit a product type.
 *
 * @param $product_type
 *   The product type array to edit or for a create form an empty product type
 *     array with properties instantiated but not populated.
 */
function finn_ui_ad_type_form($form, &$form_state, $ad_type) {
  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'finn_ui') . '/includes/finn_ui.forms.inc';

  // Store the initial product type in the form state.
  $form_state['ad_type'] = $ad_type;

  $form['ad_type'] = array(
    '#tree' => TRUE,
  );

  $form['ad_type']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $ad_type['name'],
    '#disabled' => TRUE,
    '#size' => 32,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 40,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save ad type'),
    '#submit' => array_merge($submit, array('finn_ui_ad_type_form_submit')),
  );
  $form['#validate'][] = 'finn_ui_ad_type_form_validate';

  return $form;
}

/**
 * Validation callback for commerce_product_product_type_form().
 */
function finn_ui_ad_type_form_validate($form, &$form_state) {
  $ad_type = $form_state['ad_type'];
}

/**
 * Form submit handler: save a product type.
 */
function finn_ui_ad_type_form_submit($form, &$form_state) {
  $ad_type = $form_state['ad_type'];

  // Redirect based on the button clicked.
  drupal_set_message(t('Ad type saved.'));

  $form_state['redirect'] = 'admin/finn/ads/types';
}