<?php

/**
 * @file
 * Contains the basic finn ad field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to an ad.
 */
class finn_handler_field_ad extends views_handler_field {
  function init(&$view, &$options) {
    parent::init($view, $options);

    if (!empty($this->options['link_to_ad'])) {
      $this->additional_fields['finncode'] = 'finncode';
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['link_to_ad'] = array('default' => FALSE);
    $options['link_to_ad'] = array('default' => isset($this->definition['link_to_ad default']) ? $this->definition['link_to_ad default'] : FALSE, 'bool' => TRUE);
    $options['link_to_type'] = array('default' => isset($this->definition['link_to_type default']) ? $this->definition['link_to_type default'] : 'finn');

    return $options;
  }

  /**
   * Provide the link to ad option.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['link_to_ad'] = array(
      '#type' => 'checkbox',
      '#title' => t('Link this field'),
      '#description' => t('This will override any other link you have set.'),
      '#default_value' => !empty($this->options['link_to_ad']),
    );
    $form['link_to_type'] = array(
      '#type' => 'select',
      '#title' => t('Choose the link destination'),
      '#default_value' => $this->options['link_to_type'],
      '#options' => array(
        'finn' => t('Finn.no ad'),
        'admin' => t('The ad\'s administrative view page'),
      ),
      '#states' => array(
        'visible' => array(
          ':input[name="options[link_to_ad]"]' => array('checked' => TRUE),
        ),
      ),
    );
  }

  /**
   * Render whatever the data is as a link to the ad.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_ad']) && $data !== NULL && $data !== '') {
      $finncode = $this->get_value($values, 'finncode');
      $this->options['alter']['make_link'] = TRUE;

      if ($this->options['link_to_type'] == 'admin') {
        $this->options['alter']['path'] = 'admin/finn/ads/' . $finncode;
      } else {
        $this->options['alter']['path'] = 'http://www.finn.no/finn/finncode/result?finnkode=' . $finncode;
        $this->options['alter']['external'] = TRUE;
        //$this->options['alter']['target'] = '_blank';
      }
    }

    return $data;
  }

  function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitize_value($value), $values);
  }
}
