<?php

/**
 * Filter by ad type.
 */
class finn_handler_filter_ad_type extends views_handler_filter_in_operator {
  // Display a list of ad types in the filter's options.
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Ad type');
      $this->value_options = finn_ad_type_options_list();
    }
  }
}