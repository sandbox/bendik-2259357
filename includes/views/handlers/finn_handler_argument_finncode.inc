<?php

/**
 * Argument handler to display ad titles in View using ad arguments.
 */
class finn_handler_argument_finncode extends views_handler_argument_numeric {
  function title_query() {
    $titles = array();
    $result = db_select('finn_ads', 'fa')
      ->fields('fa', array('title'))
      ->condition('fa.finncode', $this->value)
      ->execute();
    foreach ($result as $finn_ad) {
      $titles[] = check_plain($finn_ad->title);
    }
    return $titles;
  }
}
