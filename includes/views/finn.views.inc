<?php

/**
 * Implements hook_views_data()
 */
function finn_views_data() {
  $data = array();

  $data['finn_ads']['table']['group'] = t('Finn.no ad');

  $data['finn_ads']['table']['base'] = array(
    'field' => 'finncode',
    'title' => t('Finn.no ads'),
    'help' => t('Ads from finn.no.'),
    'access query tag' => 'finn_ad_access',
  );
  $data['finn_ads']['table']['entity type'] = 'finn_ad';

  // Expose the Finn Code.
  $data['finn_ads']['finncode'] = array(
    'title' => t('Finn Code'),
    'help' => t('The unique identifier for a Finn.no ad.'),
    'field' => array(
      'handler' => 'finn_handler_field_ad',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'finn_handler_argument_finncode',
    ),
  );

  // Expose the ad type.
  $data['finn_ads']['type'] = array(
    'title' => t('Type'),
    'help' => t('The human-readable name of the ad type.'),
    'field' => array(
      'handler' => 'finn_handler_field_ad_type',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'finn_handler_filter_ad_type',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['finn_ads']['attributes'] = array(
    'title' => t('Attributes'),
    'help' => t('Relates to the implementing ad type, which holds the type specific ad data.'),
    'relationship' => array(
      'label' => t('Bridge to the ads attributes'),
      'handler' => 'finn_handler_relationship_ad_type',
//      'base' => 'finn_ads_cars',
      'base_field' => 'finncode',
      'field' => 'finncode',
    ),
  );

  // Expose the ad title.
  $data['finn_ads']['title'] = array(
    'title' => t('Title'),
    'help' => t('The title of the ad used for administrative display.'),
    'field' => array(
      'handler' => 'finn_handler_field_ad',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['finn_ads']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the ad was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['finn_ads']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the ad was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['finn_ads']['scraped'] = array(
    'title' => t('Scraped date'),
    'help' => t('The date the ad was last scraped.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['finn_ads']['empty_text'] = array(
    'title' => t('Empty text'),
    'help' => t('Displays an appropriate empty text message for ad lists.'),
    'area' => array(
      'handler' => 'finn_handler_area_empty_text',
    ),
  );

  $data['finn_ads']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all the available operations links for the ad.'),
      'handler' => 'finn_handler_field_ad_operations',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function finn_views_data_alter(&$data) {
  $ad_types = finn_types();
  $ad_atts = finn_type_attributes();
  foreach ($ad_types as $type => $ad_type) {
    // Skip the ad type if it doesn't have a table...
    if (!isset($ad_type['base table'])) {
      continue;
    }
    // ... or any declared attributes.
    if (!isset($ad_atts[$type])) {
      continue;
    }
    $base_table = $ad_type['base table'];
    $base =& $data[$base_table];

    // Expose the ad type's table to views.
    $base['table']['group'] = t('Finn.no !type ad', array('!type' => $ad_type['name']));
    $base['table']['base'] = array(
      'field' => 'finncode',
      'title' => t('Finn.no @type ad attributes', array('@type' => $ad_type['name'])),
      'help' => t('Attributes related to @type type ads.', array('@type' => $ad_type['name'])),
      'access query tag' => 'finn_ad_access',
    );
    // Declare an 'implicit' relationship to the entity table, so that when
    // {finn_ads} is the base table, the attribute fields are automatically available.
    $base['table']['join'] = array(
      'finn_ads' => array(
        'left_field' => 'finncode',
        'field' => 'finncode',
      ),
    );

    // Expose the attribute fields, as defined by the implementing module.
    // @see hook_finn_type_attributes_info().
    foreach ($ad_atts[$type] as $field_name => $field) {
      if (isset($field['views field']) && !empty($field['views field'])) {
        $base[$field_name] = $field['views field'];
      }
    }
  }
}