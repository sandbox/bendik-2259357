<?php

interface FinnAdEntityControllerInterface {

  /**
   * @param $finn_ad
   *   The Finn Ad entity object.
   * @return
   *   A boolean describing the scrape result.
   */
  public function scrape($finn_ad);

}

class FinnAdEntityController extends EntityAPIController implements FinnAdEntityControllerInterface {

  public function create(array $values = array()) {
    $values += array(
      'finncode' => NULL,
      'is_new' => TRUE,
      'status' => 1,
      'title' => '',
      'uid' => '',
      'created' => '',
      'changed' => '',
    );
    return parent::create($values);
  }

  public function save($finn_ad, DatabaseTransaction $transaction = NULL) {
    global $user;

    // Hardcode the changed time.
    $finn_ad->changed = REQUEST_TIME;

    if (empty($finn_ad->{$this->idKey}) || !empty($finn_ad->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($finn_ad->created)) {
        $finn_ad->created = REQUEST_TIME;
      }
    }
    else {
      // Otherwise if the ad is not new but comes from an entity_create()
      // or similar function call that initializes the created timestamp and uid
      // value to empty strings, unset them to prevent destroying existing data
      // in those properties on update.
      if ($finn_ad->created === '') {
        unset($finn_ad->created);
      }
      if ($finn_ad->uid === '') {
        unset($finn_ad->uid);
      }
    }

    if (!empty($finn_ad->is_new) || !empty($finn_ad->scrape)) {
      try {
        $scrape_result = entity_get_controller('finn_ad')->scrape($finn_ad);
        if ($scrape_result) {
          // Set the scraped timestamp.
          $finn_ad->scraped = REQUEST_TIME;
        }
      } catch (Exception $e) {
        throw $e;
      }
    }

    return parent::save($finn_ad, $transaction);
  }

  /**
   * Implements FinnAdEntityControllerInterface::scrape().
   */
  public function scrape($finn_ad) {
    $ad_type = finn_ad_type_load($finn_ad->type);
    $data_provider = finn_get_data_provider($ad_type['data provider']);
    if (!$data_provider) {
      throw new Exception(t('No data provider available.'));
    }

    $data_store = array();
    $data = array();
    try {
      foreach (array('data_fetch', 'data_parse', 'data_apply') as $callback) {
        $function = $data_provider['callbacks'][$callback];
        if (!function_exists($function)) {
          throw new Exception(t('Data provider callback does not exist: !function', array(
            '!callback_hook' => $callback,
            '!function' => $function,
          )));
        }
        $data = $function($finn_ad, $data);
        $data_store[$callback] = $data;
        if (!isset($data) || $data === FALSE) {
          $message = t('Data provider operation `!callback_hook` failed.', array(
            '!callback_hook' => $callback,
            '!function' => $function,
          ));
          throw new Exception($message);
        }
      }
    } catch (Exception $e) {
      watchdog_exception('finn', $e);
      throw $e;
    }

    return TRUE;
  }
}