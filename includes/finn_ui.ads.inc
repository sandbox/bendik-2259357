<?php
function finn_ui_ads_overview() {
  $header = array(
    t('Finncode'),
    t('Title'),
    t('Operations'),
  );
  $rows = array();

  if (empty($rows)) {
    $rows[] = array(
      array(
        'data' => t('No ads have been added yet.'),
        'colspan' => 3,
      )
    );

  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}



function finn_ui_add_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);

  // Bypass the admin/finn/ads/create listing if only one ad type is
  // available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }

  return theme('finn_ad_create_list', array('content' => $content));
}


/**
 * Displays the list of available ad types for ad creation.
 *
 * @ingroup themeable
 */
function theme_finn_ad_create_list($variables) {
  $content = $variables['content'];
  $output = '';

  if ($content) {
    $output = '<dl class="finn-ad-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    $output = '<p>' . t('No Finn.no ad types are available.') . '</p>';
  }

  return $output;
}


/**
 * Form callback wrapper: create or edit an ad.
 *
 * @param $registry
 *   The registry object being edited by this form.
 *
 * @see eppclient_registry_registry_form()
 */
function finn_ui_ad_form_wrapper($finn_ad) {
  module_load_include('inc', 'finn', 'includes/finn.forms');
  return drupal_get_form('finn_ui_ad_form', $finn_ad);
}


/**
 * Form callback wrapper: confirmation form for deleting a registry.
 *
 * @param $registry
 *   The registry object being deleted by this form.
 *
 * @see commerce_product_product_delete_form()
 */
function finn_ui_ad_delete_form_wrapper($finn_ad) {
  module_load_include('inc', 'finn', 'includes/finn.forms');
  return drupal_get_form('finn_ui_ad_delete_form', $finn_ad);
}