<?php

/**
 * Form callback: create or edit an ad.
 *
 * @param $finn_ad
 *   The ad object to edit or for a create form an empty ad object
 *     with only a ad type defined.
 */
function finn_ad_form($form, &$form_state, $finn_ad) {
  $form_state['finn_ad'] = $finn_ad;
  $langcode = entity_language('finn_ad', $finn_ad);

  if (empty($langcode)) {
    $langcode = LANGUAGE_NONE;
  }

  //field_attach_form('finn_ad', $finn_ad, $form, $form_state, $langcode);

  $form['finncode'] = array(
    '#type' => 'textfield',
    '#title' => t('Finn.no code'),
    '#description' => t('Supply Finn.no\'s unique code for this ad.'),
    '#default_value' => $finn_ad->finncode,
    '#disabled' => empty($finn_ad->is_new),
    '#maxlength' => 128,
    '#required' => TRUE,
    '#weight' => -10,
  );

  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#description' => t('Disabled ads will not be available in the front end and may be hidden in administrative ad lists.'),
    '#options' => array(
      '1' => t('Active'),
      '0' => t('Disabled'),
    ),
    '#default_value' => $finn_ad->status,
    '#required' => TRUE,
    '#weight' => 35,
  );

  $form['data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Data'),
    '#collapsible' => FALSE,
    '#weight' => 50,
  );
  if (!empty($finn_ad->finncode) && isset($finn_ad->scraped)) {
    $form['data']['last_scrape'] = array(
      '#type' => 'markup',
      '#markup' => '<p>' . t('Last successful data retrieval: %scraped', array('%scraped' => format_date($finn_ad->scraped, 'short'))) . '</p>',
    );
    $form['data']['scrape'] = array(
      '#type' => 'checkbox',
      '#title' => t('Force data refresh'),
      '#description' => t('Refresh the data for this ad.'),
      '#access' => user_access('administer finn_ad entities'),
    );
  } else {
    $form['data']['info'] = array(
      '#type' => 'markup',
      '#markup' => '<p>' . t('Data for this ad will be populated by an external provider.') . '</p>',
    );
  }

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 400,
  );

  // Simply use default language
  $form['language'] = array(
    '#type' => 'value',
    '#value' => $langcode,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save ad'),
    '#submit' => array_merge($submit, array('finn_ad_form_submit')),
  );

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'finn_ad_form_validate';

  return $form;
}


/**
 * Validation callback for finn_ad_form().
 */
function finn_ad_form_validate($form, &$form_state) {
  $finn_ad = $form_state['finn_ad'];

  $finncode = $form_state['values']['finncode'];

  if (!is_numeric($finncode)) {
    form_set_error('finncode', t('The Finn.no code is not numeric.'));
  } else {

    if (empty($finn_ad->finncode) && (($existing_ad = finn_ad_load(trim($finncode))) !== FALSE)) {
      form_set_error('finncode', t('The Finn.no ad %finncode already exists.', array('%finncode' => $finncode)));
    }

    // TODO: check with the ad data provider for validiation here?

    // Trim leading and trailing whitespace from the Finn.no code.
    form_set_value($form['finncode'], trim($finncode), $form_state);
  }

  // Notify field widgets to validate their data.
  field_attach_form_validate('finn_ad', $finn_ad, $form, $form_state);
}

/**
 * Submit callback for finn_ad_form().
 */
function finn_ad_form_submit($form, &$form_state) {
  global $user;

  $finn_ad =& $form_state['finn_ad'];
  $finn_ad->status = $form_state['values']['status'];

  // Set the ad's uid and finncode if it's being created at this time.
  if (!empty($finn_ad->is_new)) {
    $finn_ad->finncode = $form_state['values']['finncode'];
    $finn_ad->uid = $user->uid;
  }

  if (!empty($form_state['values']['scrape'])) {
    $finn_ad->scrape = TRUE;
  }

  // Notify field widgets.
  field_attach_submit('finn_ad', $finn_ad, $form, $form_state);

  // Save the ad.
  try {
    entity_save('finn_ad', $finn_ad);

    if (!empty($form_state['values']['scrape'])) {
      drupal_set_message(t('The ad was saved successfully with new data.'));
    } else {
      drupal_set_message(t('The ad was saved successfully.'));
    }
    $form_state['redirect'] = 'admin/finn/ads';

  } catch (Exception $e) {
    $form_state['rebuild'] = TRUE;
    drupal_set_message(t('An error occoured during Finn.no data acquisition: !exception', array(
      '!exception' => $e->getMessage(),
    )), 'error');
  }
}

/**
 * Form callback: confirmation form for deleting an ad.
 *
 * @param $finn_ad
 *   The ad object to be deleted.
 *
 * @see confirm_form()
 */
function finn_ad_delete_form($form, &$form_state, $finn_ad) {
  $form_state['finn_ad'] = $finn_ad;

  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'finn_ad') . '/includes/finn.forms.inc';

  $form['#submit'][] = 'finn_ad_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $finn_ad->title)),
    '',
    '<p>' . t('Deleting this ad cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  return $form;
}

/**
 * Submit callback for finn_ad_delete_form().
 */
function finn_ad_delete_form_submit($form, &$form_state) {
  $finn_ad = $form_state['finn_ad'];

  $num_deleted = finn_ad_delete($finn_ad->finncode);
  if ($num_deleted === FALSE) {
    drupal_set_message(t('%title could not be deleted.', array('%title' => $finn_ad->title)), 'error');
  }
  else {
    drupal_set_message(t('%title has been deleted.', array('%title' => $finn_ad->title)));
  }
}
