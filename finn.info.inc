<?php

/**
 * @file
 *
 */

/**
 * Implements hook_entity_property_info().
 */
function finn_entity_property_info() {
  $info = array();

  $properties = &$info['finn_ad']['properties'];
  $properties['finncode'] = array(
    'label' => t('Finn.no Code'),
    'description' => t('The internal numeric ID of the ad.'),
    'type' => 'integer',
    'schema field' => 'finncode',
  );
  $properties['uid'] = array(
    'type' => 'integer',
    'label' => t('Owner ID'),
    'description' => t('The unique ID of the ad owner.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer finn_ad entities',
    'clear' => array('owner'),
    'schema field' => 'uid',
  );
  $properties['owner'] = array(
    'type' => 'user',
    'label' => t('Owner'),
    'description' => t('The owner of the ad.'),
    'getter callback' => 'finn_ad_get_properties',
    'setter callback' => 'finn_ad_set_properties',
    'setter permission' => 'administer finn_ad entities',
    'required' => TRUE,
    'computed' => TRUE,
    'clear' => array('uid'),
  );
  $properties['title'] = array(
    'type' => 'text',
    'label' => t('Title'),
    'description' => t('The ad title.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer finn_ad entities',
    'schema field' => 'title',
  );
  $properties['type'] = array(
    'label' => t('Type'),
    'description' => t('The ad type.'),
    'type' => 'token',
    'setter callback' => 'entity_property_verbatim_set',
    'options list' => 'finn_ad_type_options_list',
    'required' => TRUE,
    'schema field' => 'type',
  );
  $properties['status'] = array(
    'label' => t('Status'),
    'description' => t('Whether the ad is enabled or disabled.'),
    'type' => 'integer',
    'options list' => 'entity_metadata_status_options_list',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'status',
  );
  $properties['address_postcode'] = array(
    'label' => t('Postal code'),
    'description' => t('Address postal code'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'address_postcode',
  );
  $properties['address_postplace'] = array(
    'label' => t('Locality'),
    'description' => t('Address postal locality'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'address_postplace',
  );
  $properties['address_street'] = array(
    'label' => t('Street'),
    'description' => t('Address street'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'address_street',
  );
  $properties['address_country'] = array(
    'label' => t('Country'),
    'description' => t('Address country'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'address_country',
  );
  $properties['contact_name'] = array(
    'label' => t('Name'),
    'description' => t('Contact name'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'contact_name',
  );
  $properties['contact_email'] = array(
    'label' => t('E-amil'),
    'description' => t('Contact e-mail'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'contact_email',
  );
  $properties['contact_phone'] = array(
    'label' => t('Phone'),
    'description' => t('Contact phone'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'contact_phone',
  );
  $properties['contact_mobile'] = array(
    'label' => t('Mobile'),
    'description' => t('Contact mobile'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'contact_mobile',
  );
  $properties['contact_fax'] = array(
    'label' => t('Contact Fax'),
    'description' => t('Contact fax'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'contact_fax',
  );
  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the ad was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer finn_ad entities',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'label' => t('Date updated'),
    'description' => t('The date the ad was most recently updated.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'query callback' => 'entity_metadata_table_query',
    'setter permission' => 'administer finn_ad entities',
    'schema field' => 'changed',
  );
  $properties['scraped'] = array(
    'label' => t('Date fetched'),
    'description' => t('The date the ad was most recently fetched.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'query callback' => 'entity_metadata_table_query',
    'setter permission' => 'administer commerce_product entities',
    'schema field' => 'scraped',
  );

  return $info;
}

/**
 * Implements hook_entity_property_info_alter().
 */
function finn_entity_property_info_alter(&$info) {
  $ad_atts = finn_type_attributes();
  foreach ($ad_atts as $type => $attributes) {
    $properties = &$info['finn_ad']['bundles'][$type]['properties'];

    foreach ($attributes as $field_name => $field) {
      $field_pseudo_name = $field['pseudo prefix'].$field_name;
      $properties[$field_pseudo_name] = array(
        'label' => $field['label'],
        'description' => $field['description'],
        'type' => $field['type'],
        'getter callback' => $field['getter callback'],
        'setter callback' => $field['setter callback'],
        'finn_ad info' => array(
          'ad_type' => $type,
          'pseudo prefix' => $field['pseudo prefix'],
        ),
      );
    }
  }
}